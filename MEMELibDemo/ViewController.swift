//
//  ViewController.swift
//  MEMELibDemo
//
//  Created by Yuhei Miyazato on 7/23/15.
//  Copyright (c) 2015 JINS MEME. All rights reserved.
//

import UIKit

class ViewController: UITableViewController, MEMELibDelegate{
    
    var peripheralsFound = Array<CBPeripheral>()
    dynamic var stdData: MEMEStandardData!
    dynamic var rtmData: MEMERealTimeData!
    var memeMode: MEMEDataMode! = MEME_COM_REALTIME
    
    let segueName = "DataViewSegue"
    let deviceCellId = "DeviceListCellIdentifier"
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MEMELib.sharedInstance().delegate = self
        MEMELib.sharedInstance().addObserver(self, forKeyPath: "centralManagerEnabled", options: .New, context: nil)
        
        self.title = "JINS MEME DEMO"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Scan", style: .Plain, target: self, action: "scanButtonPressed:")
        self.navigationItem.rightBarButtonItem?.enabled = false
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        
        if keyPath == "centralManagerEnabled" {
            self.navigationItem.rightBarButtonItem?.enabled = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Callbacks
    func scanButtonPressed(sender:AnyObject?) {
        checkMEMEStatus(MEMELib.sharedInstance().startScanningPeripherals())
    }
    
    // MARK: - MEMELibDelegates
    func memePeripheralFound(peripheral: CBPeripheral!) {
        self.peripheralsFound.append(peripheral)
        println("peripheral found \(peripheral.identifier.UUIDString)")
        self.tableView.reloadData()
    }
    
    func memePeripheralConnected(peripheral: CBPeripheral!) {
        println("MEME Device Connected!")
        
        self.navigationItem.rightBarButtonItem?.enabled = false
        self.tableView.userInteractionEnabled = false
        
        // たまにチャタリングぽく2回はいってくることがあるので、既にDataViewControllerを表示していたら、何もしないように
        let vcs = self.navigationController?.viewControllers as! [UIViewController]
        for vc in vcs {
            if vc is DataViewController { return }
        }
        
        self.performSegueWithIdentifier(segueName, sender: self)
        MEMELib.sharedInstance().changeDataMode(memeMode)
    }
    
    func memePeripheralDisconnected(peripheral: CBPeripheral!) {
        self.navigationItem.rightBarButtonItem?.enabled = true
        self.tableView.userInteractionEnabled = true
        
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            println("MEME Device Disconnected")
        })
    }
    
    func memeStandardModeDataReceived(data: MEMEStandardData!) {
        self.stdData = data
    }

    func memeRealTimeModeDataReceived(data: MEMERealTimeData!) {
        self.rtmData = data
    }
    
    func memeDataModeChanged(mode: MEMEDataMode) {
        
        var modeStr = ""
        switch mode.value {
        case MEME_COM_UNKNOWN.value:
            modeStr = "unknown mode"
        case MEME_COM_STANDARD.value:
            modeStr = "standard mode"
        case MEME_COM_REALTIME.value:
            modeStr = "realtime mode"
        default:
            modeStr = "unknown"
        }
        
        self.willChangeValueForKey("memeMode")
        memeMode = mode
        self.didChangeValueForKey("memeMode")
        
        println("now \( modeStr )")
    }
    
    func memeAppAuthorized(status: MEMEStatus) {
        checkMEMEStatus(status)
    }
    
    // MARK: - UITableViewDataSource
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.peripheralsFound.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(deviceCellId) as? UITableViewCell
        if let deviceCell = cell {
            deviceCell.textLabel!.text = self.peripheralsFound[indexPath.row].identifier.UUIDString
        }
        
        return cell!
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var status = MEMELib.sharedInstance().connectPeripheral(self.peripheralsFound[indexPath.row])
        checkMEMEStatus(status)
        
        println("Start connecting to MEME Device...")
    }
    
    // MARK: - Utilities
    func checkMEMEStatus(status: MEMEStatus) {
        
        switch status.value {
        case MEME_ERROR.value :
            UIAlertView(title: "Error", message: "Misc error " , delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK").show()
            
        case MEME_ERROR_APP_AUTH.value :
            UIAlertView(title: "App Auth Failed", message: "Invalid Application ID or Client Secret" , delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK").show()
            
        case MEME_ERROR_SDK_AUTH.value :
            UIAlertView(title: "SDK Auth Failed", message: "Invalid SDK. Please update to the latest SDK" , delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK").show()
            
        case MEME_ERROR_CONNECTION.value :
            UIAlertView(title: "Connection Failed", message: "Some connection failure" , delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK").show()
            
        case MEME_OK.value :
            println("Status: MEME_OK")
            
        default: break
        }
    }
}
