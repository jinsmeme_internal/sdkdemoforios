//
//  DataViewController.swift
//  JINS_MEME_DUMP
//
//  Created by YuheiMiyazato on 7/1/15.
//  Copyright (c) 2015 JINS MEME. All rights reserved.
//

import Foundation
import UIKit

class DataViewController: UITableViewController {
    
    var stdData: MEMEStandardData!
    var rtmData: MEMERealTimeData!
    var currentDataMode: MEMEDataMode!
    
    let dataCellId = "dataCellIdentifier"
    var rowNum = 0;
    
    let rtmLabels = ["eyeMoveUp", "eyeMoveDown", "eyeMoveLeft", "eyeMoveRight", "blinkSpeed", "blinkStrength",
        "walking", "roll", "pitch", "yaw", "accX", "accY", "accZ", "fitError", "powerLeft"];
    let stdLabels = ["eyeMoveBigHorizontal", "eyeMoveBigVertical", "eyeMoveHorizontal", "eyeMoveVertical", "EOGNoiseDuration", "numBlinks", "numBlinksBurst", "blinkSpeed", "blinkStrength", "blinkIntervalAvg", "sleepy",
        "focus", "cadence", "bodyMoveVertical", "numSteps280", "numSteps310", "numSteps340", "numSteps370","numSteps400",
        "numSteps430", "numSteps460", "numSteps500", "numSteps530", "numSteps560", "numSteps590", "numSteps620",
        "numSteps650", "numSteps680", "numSteps710", "numSteps750", "numSteps780", "numSteps810", "numSteps840",
        "numSteps870", "numSteps900", "numSteps930", "numSteps960", "numSteps1000", "numSteps", "rollAvg",
        "pitchAvg", "rollDiff", "pitchDiff", "footholdRight", "footholdLeft", "sittingPostureIndices", "numStoredData",
        "capturedAt", "fitError", "powerLeft", "isCurrent", "isEOGValid"];
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "ModeChange", style: .Plain, target: self, action: "modeButtonPressed:")
        
        self.currentDataMode = MEMELib.sharedInstance().dataMode
        var modeStr = ""
        switch self.currentDataMode.value {
        case MEME_COM_REALTIME.value:
            modeStr = "standard"
            rowNum = self.stdLabels.count
        case MEME_COM_STANDARD.value:
            modeStr = "realtime"
            rowNum = self.rtmLabels.count
        default: break
        }
        self.title = modeStr
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        MEMELib.sharedInstance().disconnectPeripheral()
        
        // Remove Observer
        let vcs = self.navigationController?.viewControllers as! [UIViewController]
        if vcs.count<1 { return }
        
        for vc in vcs {
            if vc is ViewController {
                vc.removeObserver(self, forKeyPath: "rtmData")
                vc.removeObserver(self, forKeyPath: "stdData")
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Add Observer
        let vcs = self.navigationController?.viewControllers as! [UIViewController]
        if vcs.count<1 { return }
        
        for vc in vcs {
            if vc is ViewController {
                vc.addObserver(self, forKeyPath: "rtmData", options: .New, context: nil)
                vc.addObserver(self, forKeyPath: "stdData", options: .New, context: nil)
            }
        }
    }
    
    // MARK: - Callbacks
    func modeButtonPressed(sender: AnyObject?) {
        
        var newMode :MEMEDataMode = MEME_COM_UNKNOWN
        var modeStr = "unknown"
        self.currentDataMode = MEMELib.sharedInstance().dataMode
        
        switch self.currentDataMode.value {
            case MEME_COM_REALTIME.value:
                newMode = MEME_COM_STANDARD
                modeStr = "standard"
                rowNum = self.stdLabels.count
            case MEME_COM_STANDARD.value:
                newMode = MEME_COM_REALTIME
                modeStr = "realtime"
                rowNum = self.rtmLabels.count
            default: break
        }

        self.title = modeStr
        MEMELib.sharedInstance().changeDataMode(newMode)
    }
    
    // MARK: - KVO
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        
        switch keyPath {
            case "rtmData":
                println(object)
                self.rtmData = object.rtmData
            case "stdData":
                println(object)
                self.stdData = object.stdData
            default: break
        }
        self.tableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowNum
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(dataCellId) as? UITableViewCell
        
        var elem: (title:String?, value:String?)!
        switch self.currentDataMode.value {
            case MEME_COM_REALTIME.value:
                elem = stdValue(indexPath, stdData: self.stdData)
            case MEME_COM_STANDARD.value:
                elem = rtmValue(indexPath, stdData: self.rtmData)
            default: break
        }
        cell?.textLabel?.text = elem?.title
        cell?.detailTextLabel?.text = elem?.value
        
        return cell!
    }
    
    // MARK: - Instance methods
    private func rtmValue(ip: NSIndexPath, stdData: MEMERealTimeData?) -> (title:String?, value:String?) {
        
        if rtmData == nil { return (title:nil, value:nil) }
        
        var value = ""
        switch ip.row {
            case 0:
                value = rtmData!.eyeMoveUp.description
            case 1:
                value = rtmData!.eyeMoveDown.description
            case 2:
                value = rtmData!.eyeMoveLeft.description
            case 3:
                value = rtmData!.eyeMoveRight.description
            case 4:
                value = rtmData!.blinkSpeed.description
            case 5:
                value = rtmData!.blinkStrength.description
            case 6:
                value = rtmData!.isWalking.description
            case 7:
                value = rtmData!.roll.description
            case 8:
                value = rtmData!.pitch.description
            case 9:
                value = rtmData!.yaw.description
            case 10:
                value = rtmData!.accX.description
            case 11:
                value = rtmData!.accY.description
            case 12:
                value = rtmData!.accZ.description
            case 13:
                value = rtmData!.fitError.description
            case 14:
                value = rtmData!.powerLeft.description
            default:
                break
        }
        
        return (rtmLabels[ip.row], value)
    }
    
    private func stdValue(ip: NSIndexPath, stdData: MEMEStandardData?) -> (title:String?, value:String?) {
        
        if stdData == nil { return (title:nil, value:nil) }

        /*
        let stdLabels = ["numSteps280", "numSteps310", "numSteps340", "numSteps370","numSteps400",
        "numSteps430", "numSteps460", "numSteps500", "numSteps530", "numSteps560", "numSteps590", "numSteps620",
        "numSteps650", "numSteps680", "numSteps710", "numSteps750", "numSteps780", "numSteps810", "numSteps840",
        "numSteps870", "numSteps900", "numSteps930", "numSteps960", "numSteps1000", "numSteps", "rollAvg",
        "pitchAvg", "rollDiff", "pitchDiff", "footholdRight", "footholdLeft", "sittingPostureIndices", "numStoredData",
        "capturedAt", "fitError", "powerLeft", "isCurrent", "isEOGValid"];
        */
        
        var value = ""
        switch ip.row {
            case 0:
                value = stdData!.eyeMoveBigHorizontal.description
            case 1:
                value = stdData!.eyeMoveBigVertical.description
            case 2:
                value = stdData!.eyeMoveHorizontal.description
            case 3:
                value = stdData!.eyeMoveVertical.description
            case 4:
                value = stdData!.EOGNoiseDuration.description
            case 5:
                value = stdData!.numBlinks.description
            case 6:
                value = stdData!.numBlinksBurst.description
            case 7:
                value = stdData!.blinkSpeed.description
            case 8:
                value = stdData!.blinkStrength.description
            case 9:
                value = stdData!.blinkIntervalAvg.description
            case 10:
                value = stdData!.sleepy.value.description
            case 11:
                value = stdData!.focus.value.description
            case 12:
                value = stdData!.cadence.description
            case 13:
                value = stdData!.bodyMoveVertical.description
            case 14:
                value = stdData!.numSteps280.description
        case 15:
            value = stdData!.numSteps310.description
        case 16:
            value = stdData!.numSteps340.description
        case 17:
            value = stdData!.numSteps370.description
        case 18:
            value = stdData!.numSteps400.description
        case 19:
            value = stdData!.numSteps430.description
        case 20:
            value = stdData!.numSteps460.description
        case 21:
            value = stdData!.numSteps500.description
        case 22:
            value = stdData!.numSteps530.description
        case 23:
            value = stdData!.numSteps560.description
        case 24:
            value = stdData!.numSteps590.description
        case 25:
            value = stdData!.numSteps620.description
        case 26:
            value = stdData!.numSteps650.description
        case 27:
            value = stdData!.numSteps680.description
        case 28:
            value = stdData!.numSteps710.description
        case 29:
            value = stdData!.numSteps750.description
        case 30:
            value = stdData!.numSteps780.description
        case 31:
            value = stdData!.numSteps810.description
        case 32:
            value = stdData!.numSteps840.description
        case 33:
            value = stdData!.numSteps870.description
        case 34:
            value = stdData!.numSteps900.description
        case 35:
            value = stdData!.numSteps930.description
        case 36:
            value = stdData!.numSteps960.description
        case 37:
            value = stdData!.numSteps1000.description
        case 38:
            value = stdData!.numSteps.description
        case 39:
            value = stdData!.rollAvg.description
        case 40:
            value = stdData!.pitchAvg.description
        case 41:
            value = stdData!.rollDiff.description
        case 42:
            value = stdData!.pitchDiff.description
        case 43:
            value = stdData!.footholdRight.description
        case 44:
            value = stdData!.footholdLeft.description
        case 45:
            value = "not available"
            // value = println("sittingPostureIndices \(stdData!.sittingPostureIndices.memory)")
        case 46:
            value = stdData!.numStoredData.description
        case 47:
            value = stdData!.capturedAt.description
        case 48:
            value = stdData!.fitError.description
        case 49:
            value = stdData!.powerLeft.description
        case 50:
            value = stdData!.isCurrent.description
        case 51:
            value = stdData!.isEOGValid.description
            default:
                break
        }
        
        return (stdLabels[ip.row], value)
        
        
    }
    
}